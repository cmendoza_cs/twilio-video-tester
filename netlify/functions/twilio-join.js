const { v4: uuid } = require('uuid');
const twilioAccountSid = process.env.TWILIO_ACCOUNT_SID;
const twilioApiKey = process.env.TWILIO_API_KEY;
const twilioApiSecret = process.env.TWILIO_API_SECRET;

const createAccessToken = (roomName) => {
  const AccessToken = require('twilio').jwt.AccessToken;
  const VideoGrant = AccessToken.VideoGrant;

  // Used when generating any kind of tokens
  // To set up environmental variables, see http://twil.io/secure

  // Create Video Grant
  const videoGrant = new VideoGrant({
    room: roomName,
  });

  // Create an access token which we will sign and return to the client,
  // containing the grant we just created
  const token = new AccessToken(
    twilioAccountSid,
    twilioApiKey,
    twilioApiSecret,
    {identity: 'user-' + uuid()}
  );
  token.addGrant(videoGrant);

  // Serialize the token to a JWT string
  console.log(token.toJwt());

  return token.toJwt();
};

exports.handler = async function(event, context) {
  console.log("twilio-join request");

  if (event.httpMethod != 'POST') {
    return {
      statusCode: 405
    };
  }

  const { roomName: roomName } = JSON.parse(event.body);
  console.log("room name: ", roomName);

  const token = createAccessToken(roomName);
  console.log('participant token: ', token);

  // your server-side functionality
  return {
    statusCode: 200,
    body: JSON.stringify(token)
  };
}

