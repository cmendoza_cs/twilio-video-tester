const twilioAccountSid = process.env.TWILIO_ACCOUNT_SID;
const twilioAuthToken = process.env.TWILIO_AUTH_TOKEN;

const client = require('twilio')(twilioAccountSid, twilioAuthToken);

exports.handler = async function(event, context) {
  console.log("twilio-end request");

  if (event.httpMethod != 'POST') {
    return {
      statusCode: 405
    };
  }

  const { roomSid } = JSON.parse(event.body);
  console.log(roomSid);

  let res = null;
  await client.video.rooms(roomSid)
    .update({status: 'completed'})
    .then(room => {
      res = room;
      console.log(room.uniqueName)
    });

  // your server-side functionality
  return {
    statusCode: 200,
    body: JSON.stringify(res)
  };
}

