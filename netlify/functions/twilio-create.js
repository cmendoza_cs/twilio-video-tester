const { v4: uuid } = require('uuid');

const twilioAccountSid = process.env.TWILIO_ACCOUNT_SID;
const twilioAuthToken = process.env.TWILIO_AUTH_TOKEN;

console.log(twilioAccountSid);
console.log(twilioAuthToken);

const client = require('twilio')(twilioAccountSid, twilioAuthToken);

exports.handler = async function(event, context) {
  console.log("twilio-create request: ", event);

  if (event.httpMethod != 'POST') {
    return {
      statusCode: 405
    };
  }

  const roomName = 'vc-' + uuid();

  const { enableRecording } = JSON.parse(event.body);

  const createOptions = {
    uniqueName: roomName
  };

  if (enableRecording) {
    console.log('enabled recording');
    createOptions.recordParticipantsOnConnect = true;
  }

  let roomRes = await client.video.rooms.create(createOptions);

  console.log("roomRes: ", roomRes);

  // your server-side functionality
  return {
    statusCode: 200,
    body: JSON.stringify(roomRes)
  };
}